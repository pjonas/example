<?php
/**
 * Created by PhpStorm.
 * User: petr
 * Date: 2019-05-06
 * Time: 17:12
 */

namespace App\Facade;


use App\Entity\Project;
use App\Entity\User;
use App\Repository\ProjectRepositoryInterface;
use App\Repository\UserRepositoryInterface;

class ProjectFacade
{

	/**
	 * @var ProjectRepositoryInterface
	 */
	private $pr;
	/**
	 * @var UserRepositoryInterface
	 */
	private $ur;

	/**
	 * ProductFacade constructor.
	 *
	 * @param ProjectRepositoryInterface $pr
	 * @param UserRepositoryInterface $ur
	 */
	public function __construct(ProjectRepositoryInterface $pr, UserRepositoryInterface $ur)
	{
		$this->pr = $pr;
		$this->ur = $ur;
	}

	public function createProject(\stdClass $data): Project
	{
		$user = $this->ur->findOneActiveById((int)$data->userId);
		if(!$user){
			throw new \Exception('Wrong combination of data.');
		}
		$project = new Project();
		$project->setTitle($data->title);
		$project->setDescription($data->description);
		$project->setUser($user);
		$project->setStorage($data->storage);
		$project = $this->pr->save($project);
		return $project;
	}

    /**
     * @param $projectId
     * @param $userId
     * @param $title
     * @param $description
     * @param $storage
     * @return Project
     * @throws \Exception
     */
	public function updateProject (int $projectId,int  $userId, string $title, string $description, array $storage): Project
	{
		$project = $this->pr->findById($projectId);
		if (!$project) {
			throw new \Exception('Project not found.');
		}
		/** @var User $user */
		$user = $this->ur->findOneActiveById((int)$userId);
		if (!$user) {
			throw new \Exception('Wrong combination of data.');
		}
		if($user->getId() !== $project->getUser()->getId()) {
			throw new \Exception('Secutrity alert.');
		}
		$project->setTitle($title);
		$project->setDescription($description);
		$project->setStorage(json_encode($storage));

		return $this->pr->save($project);
	}

	public function getUserProjects(int $userId): ?iterable
    {
		$user = $this->ur->findOneActiveById($userId);
		if(!$user){
			return NULL;
		}
		return $this->pr->findUserProject($user);
	}

    /**
     * get all projects saved in application
     * @return iterable|null
     */
    public function getAllProjects(): ?iterable
    {
        return $this->pr->findAll();
    }

    /**
     * get on project
     * @param int $id
     * @return Project|null
     *
     */
	public function getProject(int $id): ?Project
	{
		return $this->pr->findById($id);
	}
}
