<?php

namespace App\Repository;


use App\Entity\Company;
use App\Entity\Project;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\Types\Object_;

class ProjectRepository extends ServiceEntityRepository implements ProjectRepositoryInterface
{
	/**
	 * UserRepository constructor.
	 *
	 * @param ManagerRegistry $registry
	 */
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, Project::class);
	}


	public function findById(int $id): ?Project
	{
		/** @var Project $project */
		$project =  $this->_em->getRepository(Project::class)->find($id);
		return $project;
	}

	public function findUserProject(User $user): iterable
	{
		$projects =  $this->_em->getRepository(Project::class)->findBy(['user'=> $user]);
		return $projects;
	}
	public function findAll(): iterable{
        return $this->_em->getRepository(Project::class)->findAll();
    }

	public function findProjectWholeCompany(Company $company): iterable
	{
		// TODO: Implement findProjectWholeCompany() method.
	}


	/**
	 * @param Project $project
	 * @return Project|null
	 */
	public function save(Project $project): ?Project
	{

		$status = TRUE;
		try{
			$this->_em->persist($project);
			$this->_em->flush();
			return $project;
		}catch (\Exception $e){
			$status = FALSE;
		}
		return null;
	}
}
