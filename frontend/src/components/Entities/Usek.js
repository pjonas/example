import Lutna from './Lutna'
import Ventilator from './Ventilator'

/**
 * Usek obsahujici ventilator,  jednotlive odporove cleny a lutnovy usek (lutna)
 * @type {{toJSON(): {ventilator: *, lutna: *, resists: *, selected: *}, addOdpor: Usek.addOdpor, computeResits: Usek.computeResits, setSelected: Usek.setSelected, getOdpory: (function(): []|*[]), setLutna: Usek.setLutna, isSelected: (function(): boolean), deleteOdpor: Usek.deleteOdpor, getLutna: (function(): {setLength: function(*=): void, setByLambda: function(*=, *=, *=): void, toJSON(): {byLambda: *, byResist: *, computedResist: *, length: *, bySelect: *}, getComputedResist: function(): Lutna.computedResist, setComputedResist: function(*=): void, getByResist: function(): Lutna.byResist, getLength: function(): Lutna.length, setByResist: function(*=): void, getBySelect: function(): Lutna.bySelect, getByLambda: function(): Lutna.byLambda, setBySelect: function(*=, *=, *=): void}|any), setVentilator: Usek.setVentilator, getVentilator: (function(): {setName: function(*): void, getName: function(): Ventilator.name, setMaximum: function(*=): void, setPressure: function(*=): void, getUser: function(): Ventilator.user, getStep: function(): Ventilator.step, getCalculateString: function(): Ventilator.calculateString, setCalculateString: function(*=, *=, *=, *=): void, getId: function(): Ventilator.id, getMaximum: function(): Ventilator.maximum, getPressure: function(): Ventilator.pressure, getMinimum: function(): Ventilator.minimun, setId: function(*): void, setFlow: function(*=): void, getFlow: function(): Ventilator.flow, setUser: function(*): void, setMinimum: function(*=): void, setStep: function(*=): void}|any)}}
 */
let Usek = {
  setLutna: function (lutna) {
    this.lutna = Object.assign(Lutna, lutna)
  },
  getLutna: function () {
    return this.lutna
  },
  setVentilator: function (ventilator) {
    this.ventilator = Object.assign(Ventilator, ventilator)
  },
  getVentilator: function () {
    return this.ventilator
  },
  addOdpor: function (odpor) {
    if (typeof this.odpory === 'undefined') {
      this.odpory = []
    }
    this.odpory.push(Object.assign({}, odpor))
  },
  deleteOdpor: function (index) {
    this.odpory.splice(index, 1)
  },
  getOdpory: function () {
    if (typeof this.odpory === 'undefined') {
      this.odpory = []
    }
    return this.odpory
  },
  computeResits: function () {
    let suma = 0
    if (typeof this.odpory === 'undefined') {
      return suma
    }
    for (let i = 0; i < this.odpory.length; i++) {
      for (let j = 0; j < this.odpory[i].odpor.odpor.length; j++) {
        if (parseInt(this.odpory[i].odpor.odpor[j].velikost) === parseInt(this.odpory[i].selectedSize)) {
          suma = suma + this.odpory[i].odpor.odpor[j].hodnota
        }
      }
    }
    return suma
  },
  setSelected: function (value) {
    this.selected = value
  },
  isSelected: function () {
    if (typeof this.selected === 'undefined') {
      this.selected = false
    }
    return this.selected
  },
  toJSON () {
    return {
      lutna: this.getLutna(),
      ventilator: this.getVentilator(),
      resists: this.getOdpory(),
      selected: this.isSelected()
    }
  }
}
export default Usek
