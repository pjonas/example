import Usek from './Usek'

/**
 * jednotlivy Projekt obsahujici momentalne jediny usek
 *
 * @type {{toJSON(): {useky: Project.useky, description: Project.description, title: Project.title, calculatorData: Project.calculatorData}, useky: [], setComputedResist: Project.setComputedResist, setUsek: Project.setUsek, description: string, setTitle: Project.setTitle, getUsek: (function(): *), calculatorData: {dul: {}, tunel: {}}}}
 */
let Project = {
  description: '',
  useky: [],
  calculatorData: {
    dul: {},
    tunel: {}
  },
  setTitle: function (title) {
    this.title = title
  },
  setUsek: function (usek) {
    this.useky[0] = Object.assign(Usek, usek)
  },
  setComputedResist: function (value) {
    this.useky[0].lutna.computedResist = parseFloat(value)
  },
  getUsek: function () {
    return this.useky[0]
  },
  toJSON () {
    return {
      title: this.title,
      description: this.description,
      useky: this.useky,
      calculatorData: this.calculatorData
    }
  }
}
export default Project
