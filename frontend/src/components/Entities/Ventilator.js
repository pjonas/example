/**
 * Entita Ventilatoru
 * id: id v Databazi
 * Name: Nazev ventilatoru
 * Flow: prutok ventilatoru
 * Pressure: tlakova hodnota ventilatoru
 * Minimum: minimalni oblast pouziti
 * Maximum: maximalni oblast pouziti
 * CalculateString: kkubicka funkce grafu ventilatory
 * Step: krok pro vypocet grafu ventilatoru
 *
 *
 * @type {{setName: Ventilator.setName, getName: (function(): string), setMaximum: Ventilator.setMaximum, setPressure: Ventilator.setPressure, getUser: (function(): number), getStep: (function(): number), getCalculateString: (function(): {a: number, b: number, c: number, d: number}|{a: *, b: *, c: *, d: *}), setCalculateString: Ventilator.setCalculateString, getId: (function(): number), getMaximum: (function(): number), getPressure: (function(): number), getMinimum: (function(): number), setId: Ventilator.setId, setFlow: Ventilator.setFlow, getFlow: (function(): number), setUser: Ventilator.setUser, setMinimum: Ventilator.setMinimum, setStep: Ventilator.setStep}}
 */
let Ventilator = {
  getId: function () {
    if (typeof this.id === 'undefined') {
      this.id = 0
    }
    return this.id
  },
  setId: function (id) {
    this.id = id
  },
  getFlow: function () {
    if (typeof this.flow === 'undefined') {
      this.flow = 0.0
    }
    return this.flow
  },
  setFlow: function (flow) {
    this.flow = parseFloat(flow)
  },
  getPressure: function () {
    if (typeof this.pressure === 'undefined') {
      this.pressure = 0.0
    }
    return this.pressure
  },
  setPressure: function (pressure) {
    this.pressure = parseFloat(pressure)
  },
  getMinimum: function () {
    if (typeof this.minimun === 'undefined') {
      this.minimun = 0.0
    }
    return this.minimun
  },
  setMinimum: function (value) {
    this.minimun = parseFloat(value)
  },
  getMaximum: function () {
    if (typeof this.maximum === 'undefined') {
      this.maximum = 0.0
    }
    return this.maximum
  },
  setMaximum: function (value) {
    this.maximum = parseFloat(value)
  },
  setUser: function (value) {
    this.user = value
  },
  getUser: function () {
    if (typeof this.user === 'undefined') {
      this.user = 0
    }
    return this.user
  },
  getStep: function () {
    if (typeof this.step === 'undefined') {
      this.step = 0.0
    }
    return this.step
  },
  setStep: function (value) {
    this.step = parseFloat(value)
  },
  getCalculateString: function () {
    if (typeof this.maximum === 'undefined') {
      this.calculateString = {
        a: 0,
        b: 0,
        c: 0,
        d: 0
      }
    }
    return this.calculateString
  },
  setCalculateString: function (ax, bx, cx, d1) {
    this.calculateString = {
      a: ax,
      b: bx,
      c: cx,
      d: d1
    }
  },
  getName: function () {
    if (typeof this.name === 'undefined') {
      this.name = ''
    }
    return this.name
  },
  setName: function (value) {
    this.name = value
  }
}

export default Ventilator
