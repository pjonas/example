export default class AdditionalCalculator {
  /**
   * Lutnovy tah netesný
   * @param k1 Koeficient netěsnosti luten
   * @param d prumer luten
   * @param l Délka lutnového tahu
   * @param r Odpor těsného lutnového tahu
   * @param l1 Délka jedné lutny
   * @param qc Potrebny objemovy prutok vetru pro odvetrani pracoviste
   * @returns {number}
   */
  lutnovyTahNetesny (k1, d, l, r, l1, qc) {
    k1 = Number(k1)
    d = Number(d)
    l = Number(l)
    r = Number(r)
    l1 = Number(l1)
    qc = Number(qc)

    /**
     * Koeficient ztrát netěsného lutnového tahu
     */
    let p = Math.pow((((k1 * d * l * Math.sqrt(r)) / (9.1 * l1)) + 1), 2)

    if (isNaN(p)) {
      return 0.0
    }
    return p * qc
  }

  /**
   * Stanoveni optimalního prumeru luten
   *
   * @param q0 Potřebný objemový průtok větrů lutnami stanovený na základě předchozích kritérii
   * @param l Celková délka lutnového tahu včetně prodloužení v důsledku tvarových a směrových změn
   * @param z Předpokládaný počet použití luten
   * @param vp Průměrný denní postup čelby za 24 hod.
   * @returns {number}
   */
  optimalniPrumerLuten (q0, l, z, vp) {
    q0 = Number(q0)
    l = Number(l)
    z = Number(z)
    vp = Number(vp)
    let numerator = 1.5835 * Math.pow(q0, 3) * l * z
    let denumerator = Math.pow(10, 6) * vp
    let result = Math.pow(numerator / denumerator, (1 / 6.53))
    if (isNaN(result)) {
      return 0.0
    }
    return result
  }

  /**
   * Propocet tlakove ztraty v lutnovem tahu
   * @param l Délka lutnového tahu
   * @param o Obvod luten
   * @param v Rychlost vetru v lutnovem tahu
   * @param k1 konstanta
   * @returns {number}
   */
  propocetTlakoveZtraty (l, o, v, k1) {
    l = Number(l)
    o = Number(o)
    v = Number(v)
    k1 = Number(k1)

    let p = l * Math.pow(v, 2) * k1

    return p
  }
  /**
   * Propocet tlakove ztraty v lutnovem tahu pro tesny lutnovy tah
   * @param r Aerodynamický odpor lutnového tahu
   * @param q Objemový průtok větrů
   * @returns {number}
   */
  propocetTlakoveZtratyTesnyTah (r, q) {
    r = Number(r)
    q = Number(q)

    return r * Math.pow(q, 2)
  }

  /**
   * Propocet tlakove ztraty v lutnovem tahu
   * @param k1 Koeficient tření
   * @param l Délka luten
   * @param d Průměr luten
   * @param ro Měrná hmotnost vzduchu
   * @param g Tíhové zrychlení
   * @param q Potřebný objemový průtok
   */
  propocetTlakoveZtratyNew (k1, l, d, ro, g, q) {
    k1 = Number(k1)
    l = Number(l)
    d = Number(d)
    ro = Number(ro)
    g = Number(g)
    q = Number(q)
    // Hmotnostní objemový průtok větrů
    let qm = ro * q
    // Obvod lutnového tahu
    let p = Math.PI * d
    let s = Math.PI * Math.pow(d / 2, 2)
    let numerator = k1 * p * l * qm
    let denumerator = 8 * Math.pow(s, 2) * Math.pow(ro, 2)
    return numerator / denumerator
  }

  /**
   * Propočet výměny vzduchu
   * @param s Svetly prurez razeneho vetraneho dila
   * @param l Delka vetraného dila
   * @param qc Vzduchovy vykon ventilatoru
   * @return {number}
   * @todo: jojo
   */
  propocetVymenyVzduchu (s, l, qc) {
    s = Number(s)
    l = Number(l)
    qc = Number(qc)

    // objem podzemniho dila
    let v = s * l
    let result = v / qc
    if (isNaN(result)) {
      return 0.0
    }
    return result
  }

  /**
   * Propočet objemového průtoku v profilu díla
   * @param r Poloměr kružnice
   * @param vn
   * @returns {number}
   */
  propocerObjemovychProfilu (r, vn) {
    r = Number(r)
    vn = Number(vn)
    let s = Math.PI * Math.pow(r, 2)
    let result = s * vn
    if (isNaN(result)) {
      return 0.0
    }
    return result
  }

  /**
   * Propocet odporu lutnoveho tahu z koeficientu treni
   * @param r odpor
   * @param p hustota vzduchu
   * @param lambda faktor treni
   * @param l delka lutnoveho tahu
   * @param d prumer luten
   * @returns {number}
   */
  propocetOdporuLutnovehoTahuZKoeficientu (r, p, lambda, l, d) {
    r = Number(r)
    p = Number(p)
    lambda = Number(lambda)
    l = Number(l)
    d = Number(d)
    let numerator = 8 * p * lambda * l
    let denumerator = Math.PI * Math.PI * Math.pow(d, 5)

    let result = numerator / denumerator
    if (isNaN(result)) {
      return 0.0
    }
    return result
  }
}
