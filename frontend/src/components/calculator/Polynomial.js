export default class Polynominal {
  compute (boda, bodb, bodc, bodd) {
    boda[0] = Number(boda[0])
    bodb[0] = Number(bodb[0])
    bodc[0] = Number(bodc[0])
    bodd[0] = Number(bodd[0])
    boda[1] = Number(boda[1])
    bodb[1] = Number(bodb[1])
    bodc[1] = Number(bodc[1])
    bodd[1] = Number(bodd[1])

    let matrixA = [
      [Math.pow(boda[0], 3), Math.pow(boda[0], 2), boda[0], -boda[1]],
      [Math.pow(bodb[0], 3), Math.pow(bodb[0], 2), bodb[0], -bodb[1]],
      [Math.pow(bodc[0], 3), Math.pow(bodc[0], 2), bodc[0], -bodc[1]],
      [Math.pow(bodd[0], 3), Math.pow(bodd[0], 2), bodd[0], -bodd[1]]
    ]
    // [a,b,c,-y]
    let matrixB = []
    // [a,b,-y]
    let matrixC = []
    // [a,-y]
    let matrixD = []
    matrixA.forEach((rowItem, index) => {
      if (index < matrixA.length && index > 0) {
        let row = []
        row[0] = rowItem[0] - matrixA[0][0]
        row[1] = rowItem[1] - matrixA[0][1]
        row[2] = rowItem[2] - matrixA[0][2]
        row[3] = rowItem[3] - matrixA[0][3]
        matrixB.push(row)
      }
    })
    matrixB.forEach((rowItem, index) => {
      let a = matrixB[index][0] / rowItem[2]
      let b = matrixB[index][1] / rowItem[2]
      let c = matrixB[index][2] / rowItem[2]
      let y = matrixB[index][3] / rowItem[2]
      // eslint-disable-next-line no-console
      matrixB[index][0] = a
      matrixB[index][1] = b
      matrixB[index][2] = c
      matrixB[index][3] = y
    })
    matrixB.forEach((rowItem, index) => {
      if (index < matrixB.length && index > 0) {
        let row = []
        // console.log(rowItem)
        row[0] = rowItem[0] - matrixB[0][0]
        row[1] = rowItem[1] - matrixB[0][1]
        row[2] = rowItem[3] - matrixB[0][3]
        matrixC.push(row)
      }
    })
    matrixC.forEach((rowItem, index) => {
      // eslint-disable-next-line no-console
      let a = matrixC[index][0] / rowItem[1]
      let b = matrixC[index][1] / rowItem[1]
      let y = matrixC[index][2] / rowItem[1]
      matrixC[index][0] = a
      matrixC[index][1] = b
      matrixC[index][2] = y
    })
    matrixC.forEach((rowItem, index) => {
      if (index < matrixC.length && index > 0) {
        let row = []
        row[0] = rowItem[0] - matrixC[0][0]
        row[1] = rowItem[2] - matrixC[0][2]
        matrixD.push(row)
      }
    })
    let parameterA = matrixD[0][1] / matrixD[0][0]
    let parameterB = Number(matrixC[0][2] - (matrixC[0][0] * parameterA))
    let parameterC = Number(matrixB[0][3] - (matrixB[0][0] * parameterA) - (matrixB[0][1] * parameterB))
    let parameterD = Number(matrixA[0][3] - (matrixA[0][0] * parameterA) - (matrixA[0][1] * parameterB) - (matrixA[0][2] * parameterC))

    let delicka = 1

    let cislo = Math.abs(Number(parameterD))
    do {
      delicka += 2
      cislo /= delicka
    }
    while (cislo > 9900)

    return {
      a: parameterA.toFixed(4),
      b: parameterB.toFixed(4),
      c: parameterC.toFixed(4),
      d: parameterD.toFixed(4)
    }
  }
}
