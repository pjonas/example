import BaseCalculator from './BaseCalculator'

/**
 * @type {{qv6: (function(*=, *=, *=, *=): number), qv5: (function(*=, *=, *=): number), qv4Hlubina: (function(*=, *=): number), qv3Saci: (function(*=, *=, *, *=, *, *): number), qv3Foukaci: (function(*=, *=, *=, *=, *=): number), qv2: (function(*=, *=, *=): number), qv4: (function(*=, *=): number)}}
 */

export default class TunelyCalculator extends BaseCalculator {
  /**
   * Qv2 - Exhalace CO2
   * @param c2 Přípustná koncentrace CO2 v projektovaném důlním díle ( % )
   * @param c3 Koncentrace CO2 ( % )
   * @param n1 Predpokladany pocet pracovniku
   * @param n2 Predpokladany pocet soucasne provozovanych naftovych stroju
   * @returns {number}
   */
  qv2a (c2, c3, n1, n2) {
    c2 = Number(c2)
    c3 = Number(c3)
    n1 = Number(n1)
    n2 = Number(n2)
    let q3 = ((0.09 * n1) + (33 * n2)) / 3600

    return (q3 / (c2 - c3)) * 100
  }

  /**
   *
   * @param t Doba potřebná k odvětrání zplodin po trhací práci
   * @param a Hmotnost odpálené trhaviny
   * @param delta Množství konvenčního CO
   * @param s Světlý průřez raženého větraného díla
   * @param l Délka větraného díla k úkrytu osádky
   * @returns {number}
   */
  qv3Foukaci (a, t, delta, s, l) {
    t = Number(t)
    a = Number(a)
    s = Number(s)
    l = Number(l)
    delta = Number(delta)

    let part1 = 0.349 / t
    let part2 = delta * a * (s * s) * (l * l)

    return part1 * Math.cbrt(part2)
  }

  /**
   * Qv3S - TP - sací
   *
   * @param a Hmotnost odpálené trhaviny ( kg )
   * @param t Doba potřebná k odvětrání zplodin po trhací práci ( min )
   * @param delta Množství konvenčního CO
   * @param s Světlý průřez raženého větraného díla ( m )
   * @param qh Měrná hmotnost horniny ( kg dm-3 )
   * @param lv Délka zabírky
   * @returns {number}
   */
  qv3Saci (a, t, delta, s, qh, lv) {
    a = Number(a)
    t = Number(t) // prepocet na sekundy
    delta = Number(delta)
    s = Number(s)
    qh = Number(qh)
    lv = Number(lv)
    let part1 = (9.417 * a) / t
    let part2 = (delta * Math.sqrt(s)) / (qh * lv)

    return part1 * Math.sqrt(part2)
  }

  qv4 (s, c) {
    s = Number(s)
    c = Number(c)

    let coef = 0
    if (c <= 0.5) {
      coef = 0.3
    }
    return coef * s
  }

  qv4Hlubina (s, c) {
    s = Number(s)
    c = Number(c)
    let coef = 0
    if (c <= 0.5) {
      coef = 0.15
    }
    return coef * s
  }

  /**
   * Qv5 - Ředění zplodin vznětových motorů
   * @param n Instalovaný výkon motoru
   * @param f Faktor průměrného vytížení ( 0,3 - převážná rovina, 0,45 - úklon v převážné části dráhy, 0,6 - silný úklon dráhy)
   * @param q Specifický objemový průtok větrů k ředění
   * @returns {number}
   */
  qv5 (n, f, q) {
    n = Number(n)
    f = Number(f)
    q = Number(q)

    let result = n * f * q
    return result
  }

  /**
   * Qv6 - Exhalace Rn
   * @param p Koeficient porušení radioaktivní rovnováhy (0,2 - 0,5)
   * @param d Předpokládanou exhalaci radonu
   * @param c Přípustna koncentraci Rn v projektovaném důlním díle
   * @param cv Koncentraci Rn v PVP před zaústěním lutnového tahu do projektovaného důlního díla
   */
  qv6 (p, d, c, cv) {
    p = Number(p)
    d = Number(d)
    c = Number(c)
    cv = Number(cv)

    return (p * d) / (1000 * (c - cv))
  }

  /**
   *
   * @param vzdalenostOdCelby
   * @param prurez
   * @param predpokladanyVyvinPrachu
   * @param maxKoncentracePrachy
   * @returns {string}
   * @constructor
   */
  VyvinPrachuComputing (vzdalenostOdCelby, prurez, predpokladanyVyvinPrachu, maxKoncentracePrachy) {
    let result = ((4 * vzdalenostOdCelby * prurez * predpokladanyVyvinPrachu) / maxKoncentracePrachy) / 3600
    return result.toFixed(2)
  }

  /**
   *
   * @param pocetPracovniku
   * @param prutokVzduchu
   * @returns {string}
   */
  nejvysspoPocetPracovniku (pocetPracovniku, prutokVzduchu) {
    let number = pocetPracovniku * prutokVzduchu
    return number.toFixed(2)
  }
}
