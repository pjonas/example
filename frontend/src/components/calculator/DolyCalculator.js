import BaseCalculator from './BaseCalculator'

/**
 * @type {{qv6: (function(*=, *=, *=, *=): number), qv5: (function(*=, *=, *=): number), qv4Hlubina: (function(*=, *=): number), qv3Saci: (function(*=, *=, *, *=, *, *): number), qv3Foukaci: (function(*=, *=, *=, *=, *=): number), qv2: (function(*=, *=, *=): number), qv1: (function(*=, *=, *=): number), qv4: (function(*=, *=): number)}}
 */

export default class DolyCalculator extends BaseCalculator {
  /**
   * Qv1 - Exhalace CH4
   * @param q1 Předpokládaná exhalace CH4 v projektovaném důlním díle ( m3s-1 )
   * @param c Přípustná koncentrace CH4 v projektovaném důlním díle ( % )
   * @param c1 Koncentrace CH4 ( % )
   * @returns {number}
   */
  qv1 (q1, c, c1) {
    q1 = Number(q1)
    c = Number(c)
    c1 = Number(c1)

    return ((100 * q1) / (c - c1))
  }

  qv4 (s, c) {
    s = Number(s)
    c = Number(c)

    let coef = 0
    if (c <= 0.5) {
      coef = 0.3
    }
    return coef * s
  }

  qv4Hlubina (s, c) {
    s = Number(s)
    c = Number(c)
    let coef = 0
    if (c <= 0.5) {
      coef = 0.15
    }
    return coef * s
  }

  /**
   * Qv6 - Exhalace Rn
   * @param p Koeficient porušení radioaktivní rovnováhy (0,2 - 0,5)
   * @param d Předpokládanou exhalaci radonu
   * @param c Přípustna koncentraci Rn v projektovaném důlním díle
   * @param cv Koncentraci Rn v PVP před zaústěním lutnového tahu do projektovaného důlního díla
   */
  qv6 (p, d, c, cv) {
    p = Number(p)
    d = Number(d)
    c = Number(c)
    cv = Number(cv)

    let result = (p * d) / (1000 * (c - cv))
    if (isNaN(result)) {
      return 0.0
    }
    return result
  }
}
